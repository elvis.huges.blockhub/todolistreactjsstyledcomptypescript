import React from "react";
import "./App.css";
import ToDo from "./app/components/ToDo";
import "./App.css";

function App() {
  return (
    <div className="App">
      <ToDo />
    </div>
  );
}

export default App;
