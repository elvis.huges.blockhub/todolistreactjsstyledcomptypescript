import styled from "styled-components";

interface StyledInputProps {
  borderRadius?: string;
}

export const StyledInput = styled.input<StyledInputProps>`
  color: palevioletred;
  border-radius: ${(props) => props.borderRadius || "10px"};
  width: 100%;
  padding: 10px;
  margin-bottom: 5px;
  font-family: Impact, Haettenschweiler, "Arial Narrow Bold";
  font-size: 20px;
  color: black;
  &.active::placeholder {
    position: absolute;
    top: 0;
    font-size: 10px;
    font-family: Impact, Haettenschweiler, "Arial Narrow Bold", sans-serif;
  }
  &::placeholder {
    font-family: Impact, Haettenschweiler, "Arial Narrow Bold", sans-serif;
  }
`;
