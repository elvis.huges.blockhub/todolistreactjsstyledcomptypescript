import React, { useState } from "react";
import { StyledInput } from "./style";

interface PropsTextInput {
  name?: string;
  value?: string;
  placeholder?: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  label?: string;
  borderRadius?: string;
}

const TextInput: React.FC<PropsTextInput> = ({
  name,
  value,
  placeholder,
  onChange,
  label,
  borderRadius,
}) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e);
  };
  const [active, setActive] = useState(false);

  return (
    <>
      <label>{label}</label>
      <StyledInput
        className={active ? "active" : ""}
        type="text"
        name={name}
        onChange={(e) => handleChange(e)}
        value={value}
        placeholder={placeholder}
        borderRadius={borderRadius}
        onFocus={() => setActive(true)}
        onBlur={() => setActive(false)}
      />
    </>
  );
};

export default TextInput;
