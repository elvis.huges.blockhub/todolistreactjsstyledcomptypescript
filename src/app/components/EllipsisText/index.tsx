import React from "react";
import { EllipsisText } from "./style";

interface PropsBaseEllipsisText {
  children: React.ReactNode;
}

const BaseEllipsisText: React.FC<PropsBaseEllipsisText> = ({ children }) => {
  return (
    <div>
      <EllipsisText>{children}</EllipsisText>
    </div>
  );
};

export default BaseEllipsisText;
