import styled from "styled-components";

//baixar icones svg https://icons8.com/icons/set/check

interface CheckboxContainerProps {
  checked: boolean;
}
interface TextProps {
  checked: boolean;
}
interface StyledCheckboxProps {
  checked: boolean;
}
interface HiddenCheckboxProps {
  checked?: boolean;
}
export const CheckboxContainer = styled.div<CheckboxContainerProps>`
  width: auto;
  height: 35px;
  padding-left: 5px;
  margin: 0px 4px;
  border-radius: 5px;

  display: flex;
  align-items: center;
`;
export const HiddenCheckbox = styled.input.attrs<HiddenCheckboxProps>({
  type: "checkbox",
})`
  display: none;
`;

export const Text = styled.label<TextProps>`
  color: ${(props) => (props.checked ? "#000" : "grey")};
`;

export const StyledCheckbox = styled.label<StyledCheckboxProps>`
  width: 30px;
  height: 30px;
  margin-right: 6px;
  border-radius: 50%;
  background: #f6f6f6;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  img {
    display: ${(props) => (props.checked ? "flex" : "none")};
    filter: invert(75%) sepia(11%) saturate(6042%) hue- rotate(30deg)
      brightness(105%) contrast(68%);
    size: 40px;
  }
`;
