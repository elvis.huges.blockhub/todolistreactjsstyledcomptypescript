import React, { useState } from "react";
import {
  CheckboxContainer,
  HiddenCheckbox,
  StyledCheckbox,
  Text,
} from "./style";

import CheckIcon from "./../../assets/images/check2.svg";

interface BaseCheckboxButtonProps {
  children?: React.ReactNode;
  checked: boolean;
  onChange: () => void;
}

const BaseCheckboxButton: React.FC<BaseCheckboxButtonProps> = ({
  children,
  checked,
  onChange,
  ...props
}) => {
  const handleCheckboxChange = () => {
    onChange();
  };

  return (
    <CheckboxContainer checked={checked} onClick={handleCheckboxChange}>
      <HiddenCheckbox onChange={handleCheckboxChange} checked={checked} />
      <StyledCheckbox checked={checked}>
        <img style={{ width: "50px" }} src={CheckIcon} />
      </StyledCheckbox>
      <Text checked={checked}>{children}</Text>
    </CheckboxContainer>
  );
};

export default BaseCheckboxButton;
