import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
  width: 100 5;
  border: 2px solid black;
  border-radius: 5px;
  background-color: #cccccc;
  margin-top: 10px;
`;
