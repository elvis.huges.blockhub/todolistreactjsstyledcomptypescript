import React from "react";
import { Container } from "./styled";
interface PropsEmptyListHeader {
  title: string;
}

export const EmptyListHeader: React.FC<PropsEmptyListHeader> = ({ title }) => {
  return <Container>{title}</Container>;
};
