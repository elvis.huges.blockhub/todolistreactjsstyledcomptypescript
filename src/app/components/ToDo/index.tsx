import React, { useState } from "react";
import { ToDo } from "../../types/ToDo";
import { DropdownItem } from "../DropdownMenu";
import ToDoList from "../ToDoList";
import ToDoListForm from "../ToDoListForm";
import { ToDoContainer, ToDoContainerBody } from "./style";

const dropdownList: Array<DropdownItem> = [
  { text: "Deletar Todas Tasks", clickId: "0" },
];

const ToDoLayout: React.FC = () => {
  const [todos, setTodos] = useState<ToDo[]>([]);

  const handleChangeState = (id: string) => {
    const changeDone = todos.map((info) => {
      if (info.id === id) {
        return { ...info, done: !info.done };
      }
      return info;
    });
    setTodos(changeDone);
  };
  const changeStateTrue = (id: string) => {
    const changeDone = todos.map((info) => {
      if (info.id === id) {
        return { ...info, done: !info.done };
      }
      return info;
    });
    setTodos(changeDone);
  };
  const handleDeleteTodo = (id: string) => {
    const newArray = todos.filter((el) => {
      return el.id !== id;
    });
    setTodos(newArray);
  };

  const createTodo = (title: string, description: string) => {
    const todo = todoFactory(title, description);
    let prevTodos = [...todos];
    prevTodos.push(todo);
    setTodos(prevTodos);
  };

  function todoFactory(title: string, description: string): ToDo {
    const newTodo: ToDo = {
      id: String(Math.floor(Math.random() * 100)),
      description: description,
      title: title,
      done: false,
    };
    return newTodo;
  }

  return (
    <div>
      <ToDoContainerBody>
        <ToDoContainer>
          <ToDoListForm createTodo={createTodo}></ToDoListForm>
          <ToDoList
            dropdownList={dropdownList}
            list={todos}
            changeTodoState={handleChangeState}
            deleteTodo={handleDeleteTodo}
            onFihishChronometerClick={changeStateTrue}
          ></ToDoList>
        </ToDoContainer>
      </ToDoContainerBody>
    </div>
  );
};

export default ToDoLayout;
