import styled from "styled-components";

export const ToDoContainer = styled.div`
  width: 550px;
  margin-top: 10px;
  border-radius: 10px;
  border: 2px solid rgba(204, 204, 204, 0.4);
  padding: 10px;
`;
export const ToDoContainerBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-content: center;
  justify-content: center;
  border-radius: 5px;
`;
