import React, { useState } from "react";
import { BaseButtonContainer } from "../BaseButton/style";
import TextInput from "../TextInput";
import { Row, Col } from "react-grid-system";
import { ToDoListFormContainer } from "./style";
import BaseButton from "../BaseButton";

//https://www.webtips.dev/solutions/get-form-values-on-submit-in-react

interface FormData {
  title: string;
  description: string;
}

interface ToDoListFormProps {
  createTodo: (title: string, description: string) => void;
}

const ToDoListForm: React.FC<ToDoListFormProps> = ({ createTodo }) => {
  const [formData, setFormData] = useState<FormData>({
    title: "",
    description: "",
  });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    createTodo(formData.title, formData.description);
    setFormData({ title: "", description: "" });
  };

  return (
    <ToDoListFormContainer>
      <form onSubmit={handleSubmit}>
        <Row nogutter>
          <TextInput
            name="title"
            placeholder="Título"
            value={formData.title}
            onChange={handleInputChange}
          ></TextInput>
        </Row>
        <Row>
          <Col sm={8}>
            <TextInput
              name="description"
              placeholder="Descrição"
              value={formData.description}
              onChange={handleInputChange}
            ></TextInput>
          </Col>
          <Col sm={4}>
            <BaseButton
              color="grey"
              borderRadius="5px"
              text="Cadastrar"
            ></BaseButton>
          </Col>
        </Row>
      </form>
    </ToDoListFormContainer>
  );
};

export default ToDoListForm;
