import styled from "styled-components";

export const ToDoListItemContainer = styled.div`
  position: relative;
  width: auto;
  min-height: 80px;
  border-radius: 10px;
  border: 2px solid rgba(204, 204, 204, 0.4);
  display: flex;
  flex-direction: row;
  margin: 10px;
  align-items: center;
  padding-left: 10px;
  .icon {
    position: absolute;
    right: 10px;
    cursor: pointer;
  }
  .infos {
    overflow-wrap: break-word;
    cursor: pointer;
  }
  .check-text {
    display: flex;
    align-items: center;
    width: 90%;
  }
`;

export const Infos = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 10px;
  max-width: 100%;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const Title = styled.p`
  font-size: 24px;
  font-weight: 600;
  margin: 0;
  text-align: start;
  font-family: Impact, "Arial Narrow Bold"; ;
`;

export const Description = styled.p`
  font-size: 18px;
  font-weight: 400;
  margin: 0;
  text-align: start;
`;
