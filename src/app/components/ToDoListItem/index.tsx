import React from "react";
import { Description, Infos, Title, ToDoListItemContainer } from "./style";

import styled from "styled-components";
import { TrashFill } from "@styled-icons/bootstrap/TrashFill";
import BaseCheckboxButton from "../BaseCheckBox";

const TrashFillContainer = styled(TrashFill)`
  color: grey;
`;

interface ToDoListITemProps {
  id: string;
  title: string;
  description: string;
  done: boolean;
  changeTodoState: () => void;
  deleteTodo: () => void;
  onInfosClick: () => void;
}

const ToDoListItem: React.FC<ToDoListITemProps> = ({
  title,
  description,
  done,
  changeTodoState,
  deleteTodo,
  onInfosClick,
}) => {
  return (
    <ToDoListItemContainer>
      <div className="check-text">
        <BaseCheckboxButton
          onChange={() => changeTodoState()}
          checked={done}
        ></BaseCheckboxButton>
        <Infos className="infos" onClick={onInfosClick}>
          <Title>{title}</Title>
          <Description>{description}</Description>
        </Infos>
      </div>

      <div className="icon">
        <TrashFillContainer onClick={deleteTodo} width={30} />
      </div>
    </ToDoListItemContainer>
  );
};

export default ToDoListItem;
