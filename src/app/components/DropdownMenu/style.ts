import styled from "styled-components";
interface DropdownContentProps {
  visible: boolean;
}

export const DropdownContainer = styled.div`
  position: relative;
  display: inline-block;
`;

export const DropdownButton = styled.button`
  background-color: white;
  color: black;
  padding: 16px;
  font-size: 16px;
  border: none;
  outline: none;
  cursor: pointer;
`;

export const DropdownContent = styled.div<DropdownContentProps>`
  display: ${(props) => (props.visible ? "block" : "none")};
  position: fixed;
  z-index: 100;
  width: 200px;
  margin-left: -65px;
  background-color: white;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  border-radius: 5px;
  margin-left: -137px;
  text-align: center;
  cursor: pointer;
`;

export const DropdownItem = styled.a`
  color: black;
  padding: 5px 5px;
  text-decoration: none;
  display: block;
  &:hover {
    background-color: #f1f1f1;
  }
`;
