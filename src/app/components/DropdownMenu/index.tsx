import { log } from "console";
import React, { useState } from "react";
import BaseButton from "../BaseButton";
import {
  DropdownButton,
  DropdownContainer,
  DropdownContent,
  DropdownItem,
} from "./style";

export interface DropdownItem {
  text: string;
  clickId: string;
}

export interface DropdownItems {
  items: Array<DropdownItem>;
  clickItem: (clickId: string) => void;
}

const DropdownMenu: React.FC<DropdownItems> = ({ items, clickItem }) => {
  const [showDropdownContent, setShowDropdownContent] =
    useState<boolean>(false);

  const handleDropdownButton = () => {
    setShowDropdownContent(!showDropdownContent);
  };

  const handleOutsideClick = (event: MouseEvent) => {
    const target = event.target as Element;
    if (showDropdownContent && !target?.closest(".drop")) {
      setShowDropdownContent(false);
    }
  };

  const handleDropdownClickItem = (itemClickId: string) => {
    clickItem(itemClickId);
  };

  React.useEffect(() => {
    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  });

  return (
    <DropdownContainer className="drop">
      <BaseButton
        text="Menu"
        padding="3px"
        fontSize="10px"
        onButtonClick={handleDropdownButton}
      />
      <DropdownContent visible={showDropdownContent}>
        {items.map((item, id) => (
          <DropdownItem
            key={id}
            onClick={() => handleDropdownClickItem(item.clickId)}
          >
            {item.text}
          </DropdownItem>
        ))}
      </DropdownContent>
    </DropdownContainer>
  );
};

export default DropdownMenu;
