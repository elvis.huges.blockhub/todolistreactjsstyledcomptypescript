import { useState } from "react";
import { Input, Label, Slider } from "./style";

interface ToDoListITemProps {
  value: string;
  checked: boolean;
  name: string;
  id: string;
  disabled: boolean;
  title: string;
  size: string;
  handleChange: (todoId: string) => void;
}

const ToggleSwitch: React.FC<ToDoListITemProps> = ({
  value,
  checked,
  name,
  id,
  disabled,
  title,
  size,
  handleChange,
}) => {
  const [isChecked, setIsChecked] = useState(false);

  const onChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    handleChange(id);
  };

  return (
    <Label htmlFor={id} disabled={disabled} title={title} size={size}>
      <Input
        id={id}
        type="checkbox"
        name={name}
        value={value}
        disabled={disabled}
        checked={checked}
        onChange={onChangeInput}
      />
      <Slider />
    </Label>
  );
};

export default ToggleSwitch;
