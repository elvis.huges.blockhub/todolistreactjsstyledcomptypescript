import styled from "styled-components";

export const ToDoListContainer = styled.div`
  max-height: 400px;
  border-radius: 10px;
  border: 2px solid rgba(204, 204, 204, 0.4);
  overflow-y: scroll;
`;

export const ToDoListHeader = styled.div`
  display: flex;
  flex-direction: column;
`;
