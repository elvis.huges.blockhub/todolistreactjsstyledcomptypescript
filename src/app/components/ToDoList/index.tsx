import React, { useState } from "react";
import { ToDo } from "../../types/ToDo";
import Chronometer from "../Chronometer";
import { DropdownItem } from "../DropdownMenu";
import { EmptyListHeader } from "../EmptyListHeader";
import ToDoListItem from "../ToDoListItem";
import { ToDoListContainer, ToDoListHeader } from "./style";

interface ToDoListProps {
  list: ToDo[];
  dropdownList?: DropdownItem[];
  changeTodoState: (todoId: string) => void;
  deleteTodo: (todoId: string) => void;
  onFihishChronometerClick: (todoId: string) => void;
}

const ToDoList: React.FC<ToDoListProps> = ({
  list,
  changeTodoState,
  deleteTodo,
  dropdownList,
  onFihishChronometerClick,
}) => {
  const [selectedTodo, setSelectedTodo] = useState<ToDo | null>(null);

  const handleFiinishClick = () => {
    onFihishChronometerClick(selectedTodo?.id || "");
    setSelectedTodo(null);
  };

  const handleInfoClick = (todo: ToDo) => {
    if (todo.id === selectedTodo?.id) {
    } else {
      setSelectedTodo(todo);
    }
  };

  return (
    <>
      {list.length ? (
        <ToDoListContainer>
          <ToDoListHeader>
            <Chronometer
              finish={handleFiinishClick}
              title={selectedTodo?.title || ""}
            />
          </ToDoListHeader>
          {/* <DropdownMenu
            items={dropdownList || []}
            clickItem={handleMenuItemclick}
          ></DropdownMenu> */}

          {list.map((item) => (
            <ToDoListItem
              key={item.id}
              id={item.id}
              title={item.title}
              description={item.description}
              done={item.done}
              changeTodoState={() => changeTodoState(item.id)}
              deleteTodo={() => deleteTodo(item.id)}
              onInfosClick={() => handleInfoClick(item)}
            />
          ))}
        </ToDoListContainer>
      ) : (
        <EmptyListHeader title="Lista Vazia" />
      )}
    </>
  );
};

export default ToDoList;
