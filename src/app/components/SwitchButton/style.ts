import styled from "styled-components";

export const RoundedCheckboxButton = styled.label`
  display: inline-block;
  width: 20px;
  height: 20px;
  border-radius: 20px;
  background-color: #fff;
  border: 2px solid #000;
  position: relative;

  &:before {
    content: "";
    display: block;
    width: 20px;
    height: 20px;
    border-radius: 10px;
    position: absolute;
    top: 10px;
    left: 10px;
    transition: all 0.3s ease;
  }

  &:after {
    content: "";
    display: block;
    width: 12px;
    height: 12px;
    border-radius: 6px;
    background-color: #000;
    position: absolute;
    top: 14px;
    left: 14px;
    opacity: 0;
    transition: all 0.3s ease;
  }
`;

export const HiddenCheckbox = styled.input.attrs({ type: "checkbox" })`
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;
