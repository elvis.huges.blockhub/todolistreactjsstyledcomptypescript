import React from "react";
import { HiddenCheckbox, RoundedCheckboxButton } from "./style";

const CheckboxButton: React.FC = (props) => {
  return (
    <RoundedCheckboxButton>
      <HiddenCheckbox {...props}></HiddenCheckbox>
    </RoundedCheckboxButton>
  );
};

export default CheckboxButton;
