import styled from "styled-components";

export const ChronometerContainer = styled.div`
  color: #fff;
  font-size: 16px;
  width: auto;
  border-radius: 5px;
  border: 2px solid black;
  margin: 10px 10px 0px 10px;
  background-color: #cccccc;
  height: auto;
`;
export const TimerContainer = styled.div`
  color: red;
  font-size: 36px;
  width: auto;
  font-family: Light;
`;
