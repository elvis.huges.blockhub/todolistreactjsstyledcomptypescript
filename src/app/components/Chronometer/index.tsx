import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-grid-system";
import BaseButton from "../BaseButton";
import { ChronometerContainer, TimerContainer } from "./style";

interface ChronometerProps {
  title: string;
  finish: () => void;
}

const Chronometer: React.FC<ChronometerProps> = ({ title, finish }) => {
  const [timeLeft, setTimeLeft] = useState(10);
  const [isRunning, setIsRunning] = useState(false);
  const [intervalId, setIntervalId] = useState<number>(0);

  useEffect(() => {
    if (isRunning) {
      let id: number = window.setInterval(() => {
        setTimeLeft(timeLeft - 1);
      }, 1000);
      setIntervalId(id);

      if (timeLeft === 0) {
        clearInterval(intervalId);
        setIsRunning(false);
        setTimeLeft(10);
      }
    } else {
      clearInterval(intervalId);
    }
    return () => clearInterval(intervalId);
  }, [isRunning, timeLeft]);

  const handleStart = () => {
    if (!title) {
      alert("Escolha uma tarefa para continuar!");
      return;
    }
    if (isRunning) {
      clearInterval(intervalId);
      setIsRunning(false);
      setTimeLeft(10);
    } else {
      setIsRunning(true);
    }
  };
  const handleStop = () => {
    if (!title) {
      alert("Nenhuma tarefa selecionada!");
      return;
    }
    setIsRunning(false);
  };
  const handleFinish = () => {
    if (!title) {
      alert("Nenhuma tarefa selecionada!");
      return;
    }
    alert(`Parabéns por finalziar a tarefa ${title}`);
    setIsRunning(false);
    setTimeLeft(10);
    finish();
  };

  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft % 60;

  return (
    <ChronometerContainer>
      <Container fluid>
        <Row justify="start">
          <Col xs={9}>
            <Row justify="start">
              <Col xs={12}>
                <Row justify="start">
                  <Col
                    xs={12}
                    style={{
                      textAlign: "start",
                      fontFamily: "fantasy",
                      color: "black",
                      height: "20px",
                    }}
                  >
                    {title ? title : "Escolha uma Tarefa!"}
                  </Col>
                </Row>
                <Row justify="center">
                  <Col xs={12} style={{ textAlign: "start" }}>
                    <TimerContainer>
                      {minutes < 10 ? `0${minutes}` : minutes}:
                      {seconds < 10 ? `0${seconds}` : seconds}
                    </TimerContainer>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col xs={3} style={{ paddingBottom: "5px" }}>
            <BaseButton
              onButtonClick={handleStart}
              text={isRunning ? "Reiniciar" : "Iniciar"}
              padding="2px"
              fontSize="10px"
              color="blue"
            />
            <BaseButton
              onButtonClick={handleStop}
              text="Parar"
              padding="2px"
              fontSize="10px"
            />
            <BaseButton
              onButtonClick={handleFinish}
              text="Finalizar Tarefa"
              padding="2px"
              fontSize="10px"
              color="red"
            />
          </Col>
        </Row>
      </Container>
    </ChronometerContainer>
  );
};

export default Chronometer;
